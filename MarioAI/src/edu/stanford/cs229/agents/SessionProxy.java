package edu.stanford.cs229.agents;

/**
 * Interface SessionProxy - a proxy to communicate with a game session.
 */
public interface SessionProxy {
    // agent params

    int MODE_TRAIN = 0;
    int MODE_TEST = 1;

    int TYPE_SIMILARITY = 0;
    int TYPE_REGULAR = 1;

    /**
     * Creates the session.
     * Note: this is a replacement for the constructor's functionality
     * and should be always called on object creation
     *
     * @param width the width of the board
     * @param height the height of the board
     */
    void create(int width, int height);

    /**
     * Runs a game session.
     */
    void run();

    /**
     * Gets the moves count in the game.
     *
     * @return the moves count
     */
    int getMovesCount();

    /**
     * Gets the progression.
     *
     * @return the progression
     */
    int getProgression();

    /**
     * Gets the wins count.
     *
     * @return the wins count
     */
    int getWinCount();

    /**
     * Gets the kills count
     *
     * @return the kills count
     */
    int getKillsCount();

    /**
     * Sets the agent's mode.
     *
     * @param agentMode the agent's mode
     */
    void setMode(int agentMode);

    /**
     * Sets the agent's type.
     *
     * @param agentType the agent's type
     */
    void setAgentType(int agentType);

    /**
     * Sets if the game should be printed.
     *
     * @param isDrawable true if the game should be printed, false otherwise
     */
    void setDrawable(boolean isDrawable);
}