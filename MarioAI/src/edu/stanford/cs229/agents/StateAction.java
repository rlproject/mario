package edu.stanford.cs229.agents;

import java.io.Serializable;

/**
 * Class StateAction - represents a state action pair.
 */
public class StateAction implements Serializable {
    private Long state;
    private int action;

    public StateAction(Long state, int action) {
        this.state = state;
        this.action = action;
    }

    public Long getState() {
        return this.state;
    }

    public int getAction() {
        return this.action;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 7 + state.hashCode();
        hash = hash * 5 + action;

        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof StateAction) {
            StateAction otherSA = (StateAction) other;
            return state.equals(otherSA.state) && action == otherSA.action;
        }

        return false;
    }
}
