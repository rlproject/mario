package edu.stanford.cs229.agents;

import java.util.HashMap;

/**
 * Class Accumulator - accumulates data.
 */
public class Accumulator {
    private HashMap<String, Double> accu;
    private HashMap<String, Integer> count;

    /**
     * Constructs the accumulator.
     */
    public Accumulator() {
        this.accu = new HashMap<>();
        this.count = new HashMap<>();
    }

    /**
     * Adds a given value to the associated id.
     *
     * @param id the id
     * @param value the value to accumulate
     */
    public void add(String id, double value) {
        if (this.accu.containsKey(id)) {
            this.accu.put(id, this.accu.get(id) + value);
            this.count.put(id, this.count.get(id) + 1);
        } else {
            this.accu.put(id, value);
            this.count.put(id, 1);
        }
    }

    /**
     * Gets the accumulated value.
     *
     * @param id the id
     * @return the accumulated value of the associated id
     */
    public Double get(String id) {
        return this.accu.get(id);
    }

    /**
     * Gets the average of the value.
     *
     * @param id the id
     * @return the average of the accumulated value
     */
    public Double  getAverage(String id) {
        return (double) this.get(id) / this.count.get(id);
    }

}
