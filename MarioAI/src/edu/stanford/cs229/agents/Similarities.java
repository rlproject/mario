package edu.stanford.cs229.agents;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class Similarities - holds the mario similarities knowledge.
 */
public class Similarities {
    public static final String FILE = "table.ser";
    private HashMap<StateAction, ArrayList<StateAction>> table;
    int debug_count = 1;
   /* public static ArrayList<Pair<MarioState, Integer>> tempSims;

    public void LoadTemp() {
        tempSims = new ArrayList<>();
        MarioState s = new MarioState();
        s.setDirection(MarioState.Direction.LEFT);
        tempSims.add(new Pair(s, MarioAction.LEFT.getActionNumber()));
    }*/

    public void setTable(HashMap<StateAction, ArrayList<StateAction>> table) {
        this.table = table;
    }

    public Similarities() {
        this.table = new HashMap<>();
    }

    public List<StateAction> getSimilars(StateAction stateAction) {
        ArrayList<StateAction> sims = this.table.get(stateAction);
        if (sims == null) { // no similars, return empty list
            sims = new ArrayList<>();
        }

        return sims;
    }

    /**
     * @param seeds - list of seed to build similarities on
     *
     */
    public void buildFromSeeds(ArrayList<FullStateAction> seeds) {
        System.out.println("Building similarities");
       // LoadTemp();
        for (FullStateAction sa : seeds) {
            ArrayList<StateAction> simList = new ArrayList<>();
            MarioState state = sa.getState();
            long key_number = state.getStateNumber();
            int key_action = sa.getAction();
            int key_direction = state.getDirection();

            // Noise on orig.
            /*ArrayList<StateAction> noiseOrig = Noise(state, key_action);
            simList.addAll(noiseOrig);*/

            // Symmetric similarity: direction and action. Then with state noise as well.
            int symmetricDirection = getSymmetricDirection(key_direction);
            int SymmetricAction = getSymmetricActionNumber(key_action);
            // No symmetry, continue.
            if (symmetricDirection == MarioState.Direction.NONE || SymmetricAction == -1) continue;
            long number = state.getStateNumber();
            simList.add(new StateAction(number, SymmetricAction));
            simList.addAll(Noise(state, SymmetricAction));

            this.table.put(new StateAction(key_number, key_action), simList);
        }

        // show total similarities
        int total = 0;
        for (HashMap.Entry<StateAction, ArrayList<StateAction> > e : this.table.entrySet()) {
            total += e.getValue().size();
        }
        System.out.println("Total similarities: " + total);
    }

    /**
     * Load similarities from file
     *
     * @param file
     */
    public void load(String file) {
        try {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            table = (HashMap<StateAction, ArrayList<StateAction> >) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Table not found");
            c.printStackTrace();
        }
    }

    /**
     * Write the similarities to file
     *
     * @param file
     */
    public void keep(String file) {
        try {
            System.out.println("Saving " + table.size() + " entries");
            FileOutputStream fout = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(table);
            oos.close();
            System.out.println("Done saving similarities");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static int getSymmetricActionNumber(int a) {
        switch(a){
            case (0):
                return 0;
            case(1): //LEFT(1, Mario.KEY_LEFT),
                return 2; // RIGHT(2, Mario.KEY_RIGHT),
            case(2):
                return 1;
            case (3):
                return -1;
            case (4):
                return -1;
            case (5): //LEFT_JUMP(5, Mario.KEY_LEFT, Mario.KEY_JUMP),
                return 6; //RIGHT_JUMP(6, Mario.KEY_RIGHT, Mario.KEY_JUMP),
            case (6):
                return 5;
            case (7): //  LEFT_FIRE(7, Mario.KEY_LEFT, Mario.KEY_SPEED),
                return 8; //  RIGHT_FIRE(8, Mario.KEY_RIGHT, Mario.KEY_SPEED),
            case (8):
                return 7;
            case (9):
                return -1;
            case  (10): //   LEFT_JUMP_FIRE(10, Mario.KEY_LEFT, Mario.KEY_JUMP, Mario.KEY_SPEED),
                return 11; // RIGHT_JUMP_FIRE(11, Mario.KEY_RIGHT, Mario.KEY_JUMP, Mario.KEY_SPEED);
            case (11):
                return 10;
            default:
                return -1;
        }
    }

    private int getSymmetricDirection(int d) {
        switch (d) {
            case(MarioState.Direction.UP_RIGHT):
                return MarioState.Direction.UP_LEFT;
            case(MarioState.Direction.UP_LEFT):
                return MarioState.Direction.UP_RIGHT;
            case(MarioState.Direction.RIGHT):
                return MarioState.Direction.LEFT;
            case(MarioState.Direction.LEFT):
                return MarioState.Direction.RIGHT;
            case(MarioState.Direction.DOWN_LEFT):
                return MarioState.Direction.DOWN_RIGHT;
            case(MarioState.Direction.DOWN_RIGHT):
                return MarioState.Direction.DOWN_LEFT;
        }
        return MarioState.Direction.NONE;
    }

    /**
     * @param state - original
     * @param action - original
     * @return list of similar StateAction with noise.
     */
    public ArrayList<StateAction> Noise(MarioState state, int action) {
        ArrayList<StateAction> noise = new ArrayList<>();
       // final int mode= state.getMarioMode();

        noise.addAll(FieldNoise(state, action));
        //noise.addAll(CollisionsWithCreaturesNoiseWrapper(state, action));

        // Noise collisionsWithCreatures
       // state.setMarioMode((mode + 1) % 2);
       // noise.addAll(CollisionsWithCreaturesNoiseWrapper(state, action));

        // Reset.
      //  state.setMarioMode(mode);
        return noise;
    }

    /**
     * @param state - original
     * @param action - original
     * @return list of similar StateAction with noise.
     */
    public ArrayList<StateAction> CollisionsWithCreaturesNoiseWrapper(MarioState state, int action) {
        ArrayList<StateAction> noise = new ArrayList<>();
        final int keyCollisionsWithCreatures= state.getCollisionsWithCreatures();
        noise.addAll(CoinsCollectedWrapperNoise(state, action));

        // Noise collisionsWithCreatures
        state.setCollisionsWithCreatures(keyCollisionsWithCreatures + 1);
        noise.addAll(CoinsCollectedWrapperNoise(state, action));
        if (keyCollisionsWithCreatures > 0) {
            state.setCollisionsWithCreatures(keyCollisionsWithCreatures - 1);
            noise.addAll(CoinsCollectedWrapperNoise(state, action));
        }

        // Reset.
        state.setCollisionsWithCreatures(keyCollisionsWithCreatures);
        return noise;
    }

    /**
     * @param state - original
     * @param action - original
     * @return list of similar StateAction with noise.
     */
    public ArrayList<StateAction> CoinsCollectedWrapperNoise(MarioState state, int action) {
        ArrayList<StateAction> noise = new ArrayList<>();
        final int keyCoinsCollected= state.getCoinsCollected();
        noise.addAll(FieldNoise(state, action));

        // Coins collected noise.
        state.setCoinsCollected(keyCoinsCollected + 1);
        noise.addAll(FieldNoise(state, action));
        if (keyCoinsCollected > 0) {
            state.setCoinsCollected(keyCoinsCollected - 1);
            noise.addAll(FieldNoise(state, action));
        }
        state.setCoinsCollected(keyCoinsCollected + 2);
        noise.addAll(FieldNoise(state, action));
        if (keyCoinsCollected > 0) {
            state.setCoinsCollected(keyCoinsCollected - 2);
            noise.addAll(FieldNoise(state, action));
        }

        // Reset coins collected.
        state.setCoinsCollected(keyCoinsCollected);
        return noise;
    }


    /**
     * @param state - original
     * @param action - original
     * @return list of similar StateAction with noise.
     */
    public ArrayList<StateAction> FieldNoise(MarioState state, int action) {
        ArrayList<StateAction> noise = new ArrayList<>();

        // Obstacle noise.
        final boolean[] o = state.getObstacles();
        state.setObstacles(ObstacleCloseNoise(o));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setObstacles(ObstacleFarNoise(o));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Reset.
        state.setObstacles(o);

        // Enemies noise
        final boolean[][] e = state.getEnemies();
        state.setEnemies(EnemiesCloseNoise(e));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setEnemies(EnemiesFarNoise(e));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Reset.
        state.setEnemies(e);

        final boolean[][] c = state.getCoins();
        state.setCoins(CoinsCloseNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setCoins(CoinsFarNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Close-Enemies and coins
        state.setEnemies(EnemiesCloseNoise(e));
        state.setCoins(CoinsCloseNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setCoins(CoinsFarNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Far-enemies and coins
        state.setEnemies(EnemiesFarNoise(e));
        state.setCoins(CoinsCloseNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setCoins(CoinsFarNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Reset
        state.setEnemies(e);
        state.setCoins(c);

        // Close-Obstacles and coins.
        state.setObstacles(ObstacleCloseNoise(o));
        state.setCoins(CoinsCloseNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setCoins(CoinsFarNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Far-obstacles and coins
        state.setObstacles(ObstacleFarNoise(o));
        state.setCoins(CoinsCloseNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));
        state.setCoins(CoinsFarNoise(c));
        noise.add(new StateAction(state.getStateNumber(), action));

        // Reset.
        state.setObstacles(o);
        state.setCoins(c);

        return noise;
    }

    /**
     * @param orig - original enemies
     * @return obstacle with noise
     */
    public boolean[] ObstacleCloseNoise(final boolean[] orig) {
        boolean[] o = new boolean[3];
        o[0] = orig[1];
        o[1] = orig[2];
        o[2] = false;
        return o;
    }

    /**
     * @param orig - original enemies
     * @return obstacle with noise
     */
    public boolean[] ObstacleFarNoise(final boolean[] orig) {
        boolean[] o = new boolean[3];
        o[0] = false;
        o[1] = orig[0];
        o[2] = orig[1];
        return o;
    }

    /**
     * @param orig - original enemies
     * @return mirror image enemies
     */
    public boolean[][] EnemiesSimetric(final boolean[][] orig) {
        boolean[][] enemies = new boolean[LearningParams.NUM_OBSERVATION_LEVELS][8];
        for (int i = 0; i < 3; i++) {
            enemies[i][MarioState.Direction.RIGHT] = orig[i][MarioState.Direction.LEFT];
            enemies[i][MarioState.Direction.LEFT] = orig[i][MarioState.Direction.RIGHT];
            enemies[i][MarioState.Direction.UP_RIGHT] = orig[i][MarioState.Direction.UP_LEFT];
            enemies[i][MarioState.Direction.UP_LEFT] = orig[i][MarioState.Direction.UP_RIGHT];
            enemies[i][MarioState.Direction.DOWN_RIGHT] = orig[i][MarioState.Direction.DOWN_LEFT];
            enemies[i][MarioState.Direction.DOWN_LEFT] = orig[i][MarioState.Direction.DOWN_RIGHT];
        }
        return enemies;
    }

    /**
     * @param orig - original enemies
     * @return enemies with noise
     */
    public boolean[][] EnemiesCloseNoise(final boolean[][] orig) {
        boolean[][] enemies = new boolean[LearningParams.NUM_OBSERVATION_LEVELS][8];
        enemies[2] = new boolean[] {false, false, false, false, false, false, false, false};
        enemies[1] = orig[2];
        enemies[0] = orig[1];
        return enemies;
    }

    /**
     * @param orig - original enemies
     * @return enemies with noise
     */
    public boolean[][] EnemiesFarNoise(final boolean[][] orig) {
        boolean[][] enemies = new boolean[LearningParams.NUM_OBSERVATION_LEVELS][8];
        enemies[0] = new boolean[] {false, false, false, false, false, false, false, false};
        enemies[1] = orig[0];
        enemies[2] = orig[1];
        return enemies;
    }

    /**
     * @param orig - original coins
     * @return coins with noise
     */
    public boolean[][] CoinsCloseNoise(final boolean[][] orig) {
        boolean[][] coins = new boolean[LearningParams.NUM_OBSERVATION_LEVELS][8];
        coins[0] = orig[1];
        coins[1] = orig[2];
        coins[2] = new boolean[] {false, false, false, false, false, false, false, false};
        return coins;
    }

    /**
     * @param orig - original coins
     * @return coins with noise
     */
    public boolean[][] CoinsFarNoise(final boolean[][] orig) {
        boolean[][] coins = new boolean[LearningParams.NUM_OBSERVATION_LEVELS][8];
        coins[0] = new boolean[] {false, false, false, false, false, false, false, false};
        coins[1] = orig[0];
        coins[2] = orig[1];
        return coins;
    }

    /**
     * Adds similarity of first to second.
     *
     * @param sa1 the first
     * @param sa2 the second
     */
    private void addSimilarityOrdered(StateAction sa1, StateAction sa2) {
        ArrayList<StateAction> sims = table.get(sa1);

        // still no similarities
        if (sims == null) {
            sims = new ArrayList<>();
            table.put(sa1, sims);
        }

        // add current
        sims.add(sa2);
    }

    /**
     * Adds symmetric similarity.
     *
     * @param sa1 one similarity
     * @param sa2 the other
     */
    public void add(StateAction sa1, StateAction sa2) {
        addSimilarityOrdered(sa1, sa2);
        addSimilarityOrdered(sa2, sa1);
    }

    // TODO(Elad): Return value can be empty, check it.
    public ArrayList<StateAction> buildSimmilarities(MarioState state, int key_action) {
        if (debug_count % 100 == 0) {
            //System.out.println(state.toString());
        } else {
            debug_count++;
        }
        ArrayList<StateAction> sims = new ArrayList<>();

        System.out.println("Building similarities");

        int key_direction = state.getDirection();
        boolean[][] key_enemies = state.getEnemies().clone();


        // Symmetric similarity: direction and action. Then with state noise as well.
        int symmetricDirection = getSymmetricDirection(key_direction);
        int SymmetricAction = getSymmetricActionNumber(key_action);
        boolean[][] symmetricEnemies = EnemiesSimetric(key_enemies);

        state.setEnemies(symmetricEnemies);
        state.setDirection(symmetricDirection);
        // No symmetry, empty return value.
        if (symmetricDirection != MarioState.Direction.NONE &&
                SymmetricAction != -1) {
            long number = state.getStateNumber();
            sims.add(new StateAction(number, SymmetricAction));
            // sims.addAll(Noise(state, SymmetricAction));

            System.out.println("StateAction similarities: " + sims.size());
        }
        state.setEnemies(key_enemies);
        state.setDirection(key_direction);
        return sims;
    }

    /**
     * Retrieves the similarity table.
     */
    public void unload() {
        this.table = new HashMap<>();
    }

    /**
     * @return the total size of the similarity map.
     */
    public int size() {
        int total = 0;
        for (HashMap.Entry<StateAction, ArrayList<StateAction> > e : this.table.entrySet()) {
            total += e.getValue().size();
        }

        return total;
    }
}
