package edu.stanford.cs229.agents;

import ch.idsia.benchmark.mario.engine.sprites.Mario;

import java.io.Serializable;

/**
 * Class FullStateAction.
 */
public class FullStateAction implements Serializable {
    private MarioState state;
    private int action;

    public FullStateAction(MarioState state, int action) {
        this.state = state;
        this.action = action;
    }

    public MarioState getState() {
        return this.state;
    }

    public int getAction() {
        return this.action;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 7 + state.hashCode();
        hash = hash * 5 + action;

        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof StateAction) {
            FullStateAction otherSA = (FullStateAction) other;
            return state.equals(otherSA.state) && action == otherSA.action;
        }

        return false;
    }
}
