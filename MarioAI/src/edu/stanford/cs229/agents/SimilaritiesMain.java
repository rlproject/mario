package edu.stanford.cs229.agents;

import ch.idsia.benchmark.tasks.LearningTask;
import ch.idsia.tools.MarioAIOptions;

import java.io.*;
import java.util.*;

/**
 * Class SimilaritiesMain - similarities builder main.
 */
public class SimilaritiesMain {

    private static Similarities build(final int RUN_COUNT) {
        Session session = new Session();
        session.create(0, 0);
        session.getAgent().track();

        for (int i = 0; i < RUN_COUNT; i++) {
            session.run();
        }

        return session.getAgent().getActionTable().getSimilarities();
    }

    public static void main(String[] args) {
        Similarities similarities = build(50);
        similarities.keep(Similarities.FILE);
        System.out.println("Total similarities: " + similarities.size());
        System.out.println("SimilaritiesMain end");
    }
}
