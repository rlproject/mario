package edu.stanford.cs229.agents;

import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.tasks.BasicTask;
import ch.idsia.tools.EvaluationInfo;
import ch.idsia.tools.MarioAIOptions;

/**
 * Class Session - belongs to the operator. Temporally here.
 */
public class Session implements SessionProxy {
    private MarioRLAgent agent;
    private Similarities sim;
    private BasicTask basicTask;
    private EvaluationInfo results;
    private boolean isDrawable;

    /**
     * Constructs the session.
     */
    public Session() {
        this.sim = null;
        this.isDrawable = false;
    }

    public void create(final int WIDTH, int HEIGHT) {
        if (this.sim == null) {
            this.sim = new Similarities();
        }

        agent = new MarioRLAgent(sim);
    }

    /**
     * Runs the game session.
     */
    public void run() {
        MarioAIOptions marioAIOptions = new MarioAIOptions();
        marioAIOptions.setAgent(agent);
        marioAIOptions.setVisualization(true);
        marioAIOptions.setFPS(24);
        marioAIOptions.setMarioMode(2);
        marioAIOptions.setVisualization(isDrawable);
        basicTask = new BasicTask(marioAIOptions);
        initialize();
        basicTask.runSingleEpisode(1);
        results = basicTask.getEvaluationInfo();
    }

    /**
     * Initializes the session.
     */
    private void initialize() {
        agent.init();
        agent.reset();
        basicTask.reset();

        if (agent.getMode() == SessionProxy.MODE_TRAIN) {
            agent.getActionTable().learningRate = 0.8f;
            agent.getActionTable().explorationChance = 0.3f;
        } else { // test
            agent.getActionTable().learningRate = 0.0f;
            agent.getActionTable().explorationChance = 0.01f;
        }
    }

    /**
     * Gets the moves count.
     *
     * @return the moves count
     */
    public int getMovesCount() {
        return Integer.MIN_VALUE;
    }

    @Override
    public int getProgression() {
        return results.distancePassedCells;
    }

    @Override
    public int getWinCount() {
        return results.marioStatus == Mario.STATUS_WIN ? 1 : 0;
    }

    @Override
    public int getKillsCount() {
        int r;
        try {
            r = results.computeKillsTotal();
            return  r;
        } catch (Exception e) {
            // e.printStackTrace();
            return -1;
        }
    }

    @Override
    public void setMode(int agentMode) {
        agent.setMode(agentMode);
    }

    @Override
    public void setAgentType(int agentType) {
        if (agentType == TYPE_SIMILARITY) {
            this.sim.load("table.ser");
        } else {
            this.sim.unload();
        }
    }

    @Override
    public void setDrawable(boolean isDrawable) {
        this.isDrawable = isDrawable;
    }

    public MarioRLAgent getAgent() {
        return this.agent;
    }
}

