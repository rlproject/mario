package edu.stanford.cs229.agents;

/**
 * Class Main.
 * Runs the Mario domain with MarioRLAgent.
 * Runs batches of train and test, results in the averages of progress, kills and wins.
 */
public class Main {
    private static final String PROGRESS = "progress";
    private static final String KILLS = "kills";
    private static final String WINS = "wins";
    private static final String NULL = "null";

    private static Accumulator run(Session session, final int TRAIN_TOTAL) {
        Accumulator accumulator = new Accumulator();
        int trainCount = 1;
        final int BATCH = TRAIN_TOTAL * 10 / 100;

        System.out.println("Begin");

        while (trainCount <= TRAIN_TOTAL) {
            // run a session
            session.run();

            accumulator.add(KILLS, 0);
            accumulator.add(PROGRESS, 0);
            accumulator.add(WINS, 0);

            // show avg
            if (trainCount % BATCH == 0) {
                System.out.println(String.format("Avg kills: %.2f\tAvg Progress: %.2f\tTotal wins: %.2f",
                                                accumulator.getAverage(KILLS), accumulator.getAverage(PROGRESS),
                                                accumulator.getAverage(WINS)));

                if (trainCount < TRAIN_TOTAL) {
                    accumulator = new Accumulator();
                }
            } else {
                int kills = session.getKillsCount();

                if (kills == -1) {
                    accumulator.add(NULL, 1);
                } else {
                    accumulator.add(NULL, 0);
                    accumulator.add(KILLS, kills);
                    accumulator.add(PROGRESS, session.getProgression());
                    accumulator.add(WINS, session.getWinCount());
                }

            }

            trainCount++;
        }

        System.out.println("End training");

        return accumulator;
    }

    /**
     * main function.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        final int TEST_COUNT = 10, TEST_TOTAL = 1000, TRAIN_TOTAL = 100;
        Session session = new Session();
        session.create(0, 0);
        session.setAgentType(SessionProxy.TYPE_SIMILARITY);

        // RUN TRAINING
        run(session, TRAIN_TOTAL);

        // RUN EVALUATION
        Accumulator avg = new Accumulator();
        System.out.println("----- Running Evaluation -----");
        session.setMode(SessionProxy.MODE_TEST);
        for (int i = 1; i <= TEST_COUNT; i++) {
            Accumulator current = run(session, TEST_TOTAL / TEST_COUNT);
            avg.add(KILLS, current.getAverage(KILLS));
            avg.add(PROGRESS, current.getAverage(PROGRESS));
            avg.add(WINS, current.getAverage(WINS));
        }
        System.out.println(String.format("Avg kills: %.2f\tAvg Progress: %.2f\tTotal wins: %.2f",
                avg.getAverage(KILLS), avg.getAverage(PROGRESS), avg.getAverage(WINS)));
    }
}
